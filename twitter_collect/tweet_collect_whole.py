file_path = "/Users/gabrielkikano/Documents/CentraleSupélec/CodingWeek_Docs/"

def get_candidate_queries(num_candidat,file_path):
    try:
        with open(file_path+"keywords_candidate_"+str(num_candidat)+".txt", "r") as mots: #parcours des fichiers contenant les mots clés
            with open(file_path+"hastag_candidate_"+str(num_candidat)+".txt", "r") as com: #parcours des fichiers contenant les hastags
                Mots =mots.read() #ensemble de transformations des fichiers en objets utilisables par python
                keywords = Mots.split(" ")
                Com =com.read()
                hastag = Com.split(" ")
                s="" #la chaine de caractères s contiendra tous les mots et hashtags des fichiers en entrée en les séparant par "OR" afin de tous les relever dans la fonction collect_candidate_actuality_retweet
                for i in range (len(keywords)):
                    s+=keywords[i]+" "+"OR"+" " 
                for i in range (len(hastag)-1):
                    s+=hastag[i]+" "+"OR"+" "
                return s+hastag[len(hastag)-1]
    except IOError:
        print("IOError")




