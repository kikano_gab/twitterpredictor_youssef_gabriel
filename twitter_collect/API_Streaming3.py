from tweepy.streaming import StreamListener
import datetime
from tweepy.streaming import StreamListener
import tweepy
import twitter_collect.twitter_connection_setup as twit
import json
from tweepy.streaming import StreamListener
import tweepy
import twitter_collect.twitter_connection_setup as twit
import datetime
import json



class FileWriteListener(StreamListener):

    def __init__(self):
        super(StreamListener, self).__init__()
        self.save_file = open('tweets.json','w')
        self.tweets = []
        self.time_init = datetime.datetime.now()

    def on_data(self, tweet):
        if (datetime.datetime.now() - self.time_init) >= datetime.timedelta(0, 10):
            return (False)
        else:
            self.tweets.append(json.loads(tweet))
            self.save_file.write(str(tweet))

    def on_error(self, status):
        print(status)
        return True

def collect_by_streaming3():
    connexion = twit.twitter_setup()
    listener = FileWriteListener()
    stream=tweepy.Stream(auth = connexion.auth, listener=listener)
    stream.filter(track=['Emmanuel Macron'])

