import panda as pd
import twitter_collect.tweet_collection as s
import twitter_collect.twitter_connection_setup as twit


connexion = twit.twitter_setup()
status = connexion.search("Emmanuel Macron",language="french",rpp=100)
files= "resultats.json"


def dataframe(status, filename,reference_to):
    s.store_tweets(status,filename,reference_to)
    with open (filename,"r") as tweet:
        df = pd.DataFrame({ 'Contenu' : tweet[0],
                            'Date' : tweet[1],
                            'Id' : tweet[3],
                            'Hastag' : tweet[2],
                            'User_id' : tweet[4]})
    tweet.close()
    return df


