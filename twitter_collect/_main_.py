import twitter_collect.tweet_collect_whole as tw
import twitter_collect.API_search as t
import twitter_collect.twitter_connection_setup as twit
import twitter_collect.API_Streaming as stream
import twitter_collect.API_Users as us
import twitter_collect.collect_candidate_tweet_activity as q
import twitter_collect.collect_candidate_actuality_tweets as col

candidat = ['Emmanuel Macron', 1976143068]
file_path = "/Users/gabrielkikano/Documents/CentraleSupélec/CodingWeek_Docs/"
num_candidat = 1
queries=tw.get_candidate_queries(num_candidat,file_path)
twit_api = twit.twitter_setup()

def main(queries,twit_api,candidat):
    col.get_tweets_from_candidates_search_queries(queries,twit_api)
    q.collect_tweet_candidat(candidat)

