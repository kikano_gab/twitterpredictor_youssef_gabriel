from tweepy.streaming import StreamListener
import tweepy
import twitter_collect.twitter_connection_setup as twit
import datetime
import json
#
#class StdOutListener(StreamListener):
class TweepyFileListener(tweepy.StreamListener): #Obligé de changer de lsitener pour pouvoir stocker les tweets

    def __init__(self):
        self.time_init = datetime.datetime.now() #On se donne 10secondes pour collecter les tweets sur le sujet

    def on_data(self, data):


        if (datetime.datetime.now() - self.time_init) >= datetime.timedelta(0, 10): #Si pas de tweet pendant les 10secondes on retourne False
            return False

        else:
            print ("on_data called") #Twitter retourne des données sous format JSON, on a donc d'abord besoin de le décoder

            decoded = json.loads(data)
            msg = '@%s: %s\n' % (
                decoded['user']['screen_name'],
                decoded['text'].encode('ascii', 'ignore'))

            with open("Output.txt", "a") as tweet_log: #On créé un fichier .txt pour stocker les tweets du flux dedans
                print ("Received: %s\n" % msg)
                tweet_log.write(msg)

    def on_error(self, status):
        if  str(status) == "420":
            print(status)
            print("You exceed a limited number of attempts to connect to the streaming API")
            return False
        else:
            return True
#



def collect_by_streaming():
    connexion = twit.twitter_setup()
    listener = TweepyFileListener()
    stream=tweepy.Stream(auth = connexion.auth, listener=listener)
    stream.filter(track=['Emmanuel Macron'])

 #ce code permet de fournir en direct les tweets contenant la chaine de caractères Emmanuel Macron
