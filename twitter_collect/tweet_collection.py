import json
import twitter_collect._main_ as m
import twitter_collect.tweet_collect_whole as tw
import twitter_collect.twitter_connection_setup as twit
import twitter_collect.API_Streaming as streamin
import numpy as np
import os
import tweepy


connexion = twit.twitter_setup()
tweets = connexion.search("Emmanuel Macron",language="french",rpp=100)
files= "resultats.json"

def store_tweets(tweets,filename,reference_to):
    """
    Stores a list of tweets in the file filename
    :param tweets: the tweets to store
    :param filename: the file in which the tweets will be stored
    :param reference_to: the candidate or personality the tweet is a reference to


    """
    stored_tweets={}
    if os.path.exists(filename):
        read_file = open(filename, 'r')
        stored_tweets = json.load(read_file)
        read_file.close()
    write_file = open(filename, 'w')
    for tweet in tweets :
        dict = tweet.__json
        custom_tweet = {
            "text": dict["text"],
            "date": dict["created_at"],
            "hastags": [hastag["text"] for hastag in dict["entities"]["hastags"]],
            "id": np.int64(dict["id"]),
            "user_id:": np.int64(dict["user"]["id"])
        }
        if str(reference_to in stored_tweets.keyx()):
            stored_tweets[str(reference_to)].append(custom_tweet)
        else:
            stored_tweets[str(reference_to)] = [custom_tweet]
            json.dump(stored_tweets, write_file, indent = 4)
            write_file.close()






# candidat = ['Emmanuel Macron', 1976143068]
# file_path = "/Users/gabrielkikano/Documents/CentraleSupélec/CodingWeek_Docs/"
# num_candidat = 1
# queries=tw.get_candidate_queries(num_candidat,file_path)
# twit_api = twit.twitter_setup()
# # tweets = m.main(queries,twit_api,candidat)


# def store_tweets(file_name):
#     with open("Output.txt", "r") as tweet_texte : #On convertit les tweets qui sont en format .txt en données de format string
#         data = tweet_texte.read()
#         data.split('\n')
#     with open(file_name, "w") as write_file:
#         json.dump(data, write_file, indent=4) #Sérialisation des tweets avec JSON


# def store_tweets(file_name,tweets):
#     with open (file_name,"w") as write_file:
#         json.dumps(tweets, write_file, indent=4)









