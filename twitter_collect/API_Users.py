import twitter_collect.twitter_connection_setup as twit

def collect_by_user(user_id):
    connexion = twit.twitter_setup()
    statuses = connexion.user_timeline(id = user_id, count = 20)
    for status in statuses:
        print(status.text)
    return statuses
