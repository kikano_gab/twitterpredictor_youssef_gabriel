from tweepy.streaming import StreamListener
import datetime
from tweepy.streaming import StreamListener
import tweepy
import twitter_collect.twitter_connection_setup as twit
import json

class StdOutListener(StreamListener):

    def __init__(self):
        self.time_init = datetime.datetime.now()

    def on_data(self, data):
        print ("on_data called")
        if (datetime.datetime.now() - self.time_init) >= datetime.timedelta(0, 10):
            return (False)
        else:
            print(data.__json)
        return True





    def on_error(self, status):
        if  str(status) == "420":
            print(status)
            print("You exceed a limited number of attempts to connect to the streaming API")
            return False
        else:
            return True





def collect_by_streaming2():
    connexion = twit.twitter_setup()
    listener = StdOutListener()
    stream=tweepy.Stream(auth = connexion.auth, listener=listener)
    stream.filter(track=['Emmanuel Macron'])
