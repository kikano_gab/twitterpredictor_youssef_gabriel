import panda as pd
import twitter_collect.twitter_connection_setup as twit
import twitter_collect.dataframe_creation as df
import matplotlib.pyplot as plt

connexion = twit.twitter_setup()
status = connexion.search("Emmanuel Macron",language="french",rpp=100)
files= "resultats.json"
data=df.dataframe(status,files,"Macron")

tfav = pd.Series(data=data['Likes'].values, index=data['Date'])
tret = pd.Series(data=data['RTs'].values, index=data['Date'])
tlong = pd.Series(data=data['len'].values, index=data['Date'])

# Graphique représentant la comparaison entre le nombre de likes et la longueur du tweet (mise en évidence d'un lien de cause à effet):
def len_fav():
    tfav.plot(figsize=(16,4), label="Likes", legend=True)
    tlong.plot(figsize=(16,4), label="Longueur", legend=True)

# Graphique représentant le nombre de likes en fonction de la longueur du tweet:
def ret_fav():
    tfav.plot(figsize=(16,4), label="Likes", legend=True)
    tret.plot(figsize=(16,4), label="Retweets", legend=True)

plt.show()

