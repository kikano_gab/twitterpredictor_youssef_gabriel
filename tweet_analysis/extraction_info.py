import panda as pd
import twitter_collect.twitter_connection_setup as twit
import twitter_collect.dataframe_creation as df
import matplotlib.pyplot as plt
import numpy as np

connexion = twit.twitter_setup()
status = connexion.search("Emmanuel Macron",language="french",rpp=100)
files= "resultats.json"
data=df.dataframe(status,files,"Macron")

#Déclaration des variables donnant le nombre de likes maximal et le nombre de retweet maximal
rt_max  = np.max(data['RTs'])
rt  = data[data.RTs == rt_max].index[0]
likes_max = np.max(data['Likes'])
likes = data[data.Likes == likes_max].index[0]

# Max retweets:

print("The tweet with more retweets is: \n{}".format(data['tweet_textual_content'][rt]))
print("Number of retweets: {}".format(rt_max))

#Max tweets favoris:

print("The tweet with more likes is: \n{}".format(data['tweet_textual_content'][likes]))
print("Number of likes: {}".format(likes_max))

#Fonction qui compare la popularité du tweet faisant le plus de retweets et le tweet qui fait le plus de likes:
def popularite(rt_max,likes_max):
    if rt_max > likes_max:
        print("Le tweet fait parler mais n'est pas populaire")
    elif likes_max <= rt_max :
        print("Le tweet est populaire")


print("{} characters.\n".format(data['len'][rt]))
